const { port} = require("./config.js");
const http = require("http");
const fs = require("fs");
const uuID = require("crypto");
const path = require("path");
const htmlFilePath = path.join(__dirname, "data.html");
const jsonFilePath = path.join(__dirname, "data.json");



function workingWithHttp() {

  const server = http.createServer((request, response) => {

    if (request.method === "GET" && request.url.toLocaleLowerCase() === '/html') {

      fs.readFile(htmlFilePath, "utf8", (err, data) => {
        if (err) {
          console.error(err);

        } else {
          response.setHeader("Content-Type", "text/html");
          response.end(data);

        }
      });

    } else if (request.method === "GET" && request.url.toLocaleLowerCase() === '/json') {

      fs.readFile(jsonFilePath, "utf8", (err, data) => {
        if (err) {
          console.error(err);

        } else {
          response.setHeader("Content-Type", "application/json");
          response.end(data);

        }
      });

    } else if (request.method === "GET" && request.url.toLocaleLowerCase() === '/uuid') {
      const uuIDV4 = uuID.randomUUID();

      response.end(`{"uuid": ${uuIDV4}}`);

    } else if (request.method === "GET" && request.url.toLocaleLowerCase().startsWith('/status')) {

      const code = request.url.split("/")[2];
      response.setHeader("Content-Type", "text/plain");
      const statusCode = http.STATUS_CODES;

      if (code in statusCode) {
        response.end(
          ` ${code} : ${statusCode[code]} `
        );

      } else {
        response.end(
          `Status Code is not valid : Bad Request `
        );

      }
    } else if (request.method === "GET" && request.url.toLocaleLowerCase().startsWith('/delay')) {
      const delayTime = +request.url.split("/")[2];
      response.setHeader("Content-Type", "text/plain");

      if(typeof delayTime === 'number' && delayTime >= 0){

        setTimeout(() => {
          response.end(
            `Response sent after:${delayTime} seconds `
          );
        }, delayTime * 1000);

      } else {
        response.end(
          `Enter a Valid Response time`);
      }
      
    } else if (request.url === '/'){
      response.setHeader("Content-Type", "text/html");
      response.end(
        `<!DOCTYPE html>
        <html>
        <head>
        </head>
        <body>
        <h3> Home Page </h3>
        <ul>
        <li> /html </li>
        <li> /json </li>
        <li> /uuid </li>
        <li> /status/enter_status_codes </li>
        <li> /delay/enter_seconds </li>
        </ul>
        </body>
        </html>`
      );
    } else {

      response.setHeader("Content-Type", "text/plain");
      response.end( `Page Not Found`);
      
    }
  });

  server.listen(port, () => {
    console.log(`server is on live `);
  });
  
}

workingWithHttp();
